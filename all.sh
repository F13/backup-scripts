#!/bin/sh

SCRIPTS_PATH=/var/backup-scripts/
export PATH=${PATH}:${SCRIPTS_PATH}
cd ${SCRIPTS_PATH}

LOG_PATH=/var/log/backup-scripts/
mkdir -p ${LOG_PATH}

if hash psql 2>/dev/null; then
    ./postgresql.sh >>${LOG_PATH}postgresql.txt  2>&1
fi
./directories.sh    >>${LOG_PATH}directories.txt 2>&1
./log.sh            >>${LOG_PATH}log.txt         2>&1
