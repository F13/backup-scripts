Backup scripts
==============

Usage
-----
- scripts directory
    - `chmod 600 ./*.sh`
    - `./deploy.sh`
    - `chmod 700 ./*.sh`
    - specify current directory in `all.sh` `SCRIPTS_PATH`
- specify `LOG_PATH` in `all.sh`
- specify `BACKUP_PATH` in all *.sh
- fill `directories.csv` in this format `blabla-media,/var/www/blabla/common/media/`
- `mv ./all.sh /etc/cron.daily/backup.sh`
