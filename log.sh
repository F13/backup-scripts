#!/bin/sh

BACKUP_PATH=/var/backup/
EXPIRED_DAYS=30

if hash nginx 2>/dev/null; then
    directory_destination=${BACKUP_PATH}nginx
    mkdir -p ${directory_destination}

    if     ls /var/log/nginx/*.gz              1> /dev/null 2>&1; then
        mv -v /var/log/nginx/*.gz              ${directory_destination}
    fi

    find ${directory_destination} -mtime +${EXPIRED_DAYS} -exec rm -rf '{}' ';' 2>/dev/null
fi
if hash php-fpm 2>/dev/null; then
    directory_destination=${BACKUP_PATH}php
    mkdir -p ${directory_destination}

    if     ls /var/log/php-fpm/error.log-*     1> /dev/null 2>&1; then
        mv -v /var/log/php-fpm/error.log-*     ${directory_destination}
    fi
    if     ls /var/log/php-fpm/www-error.log-* 1> /dev/null 2>&1; then
        mv -v /var/log/php-fpm/www-error.log-* ${directory_destination}
    fi

    find ${directory_destination} -mtime +${EXPIRED_DAYS} -exec rm -rf '{}' ';' 2>/dev/null
fi
