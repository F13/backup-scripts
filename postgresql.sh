#!/bin/sh

DB_HOST=
BACKUP_PATH=/var/backup/
DAYS_TO_KEEP=7
WEEKS_TO_KEEP=5
DAY_OF_WEEK_TO_KEEP=5

export LC_NUMERIC="en_US.UTF-8"
FONT_RESET=$(tput sgr0)
COLOR_GREEN=$(tput setf 2)
COLOR_RED=$(tput setf 4)
CURSOR_END=$(tput hpa $(tput cols))$(tput cub 6)

if [ -z "${DB_HOST}" ]; then
    DB_HOST=''
else
    DB_HOST=' -h "'${DB_HOST}'"'
fi;
DB_DIRECTORY=${BACKUP_PATH}'db/'
mkdir -p ${DB_DIRECTORY}

function perform_backups()
{
    DB_QUERY='SELECT datname FROM pg_database WHERE NOT datistemplate AND datallowconn AND datname != '\''postgres'\'' AND datname NOT LIKE '\''%_development'\'' ORDER BY datname;'
    DB_SUFFIX=`date +\%Y-\%m-\%d`$1'.backup'
    for db_name in `psql${DB_HOST} -U 'postgres' -At -c "${DB_QUERY}" postgres`
    do
        time_start=$(date +%s.%N)

        db_file=${DB_DIRECTORY}${db_name}${DB_SUFFIX}
        db_file_temp=${db_file}'.in_progress'

        echo -n '* '${db_name}
        if ! pg_dump -Fc${DB_HOST} -U 'postgres' ${db_name} -f ${db_file_temp}; then
            echo -n ' '${COLOR_RED}'fail'${FONT_RESET}'ed'
        else
            mv ${db_file_temp} ${db_file}

            echo -n ' completed'
        fi
        time_duration=$(echo "$(date +%s.%N) - $time_start" | bc)
        printf " in %.3f seconds\n" $time_duration
    done
}

# MONTHLY BACKUPS

DAY_OF_MONTH=`date +%d`
if [ ${DAY_OF_MONTH} -eq 1 ]; then
    find ${DB_DIRECTORY} -maxdepth 1 -name '*-monthly.backup' -exec rm -rf '{}' ';' 2>/dev/null
    perform_backups '-monthly'

    exit 0;
fi

# WEEKLY BACKUPS

DAY_OF_WEEK=`date +%u`
if [ ${DAY_OF_WEEK} = ${DAY_OF_WEEK_TO_KEEP} ]; then
    EXPIRED_DAYS=`$(( (${WEEKS_TO_KEEP} * 7) + 1 ))`

    find ${DB_DIRECTORY} -maxdepth 1 -mtime +${EXPIRED_DAYS} -name '*-weekly.backup' -exec rm -rf '{}' ';' 2>/dev/null
    perform_backups '-weekly'

    exit 0;
fi

# DAILY BACKUPS

find ${DB_DIRECTORY} -maxdepth 1 -mtime +${DAYS_TO_KEEP} -name '*-daily.backup' -exec rm -rf '{}' ';' 2>/dev/null
perform_backups '-daily'
