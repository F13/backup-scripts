#!/bin/sh

BACKUP_PATH=/var/backup/
DIRECTORY_INCREMENTAL_PREFIX=deleted-
EXPIRED_DAYS=30

export LC_NUMERIC="en_US.UTF-8"
FONT_RESET=$(tput sgr0)
COLOR_RED=$(tput setf 4)
WEIGHT_BOLD=$(tput bold)

IFSOLD=$IFS
IFS=,
cat /var/backup-scripts/directories.csv | while read directory_name directory_source; do
    time_start=$(date +%s.%N)

    directory_destination=${BACKUP_PATH}${directory_name}/
    mkdir -p ${directory_destination}
    echo -e "\n"${WEIGHT_BOLD}${directory_name}${FONT_RESET}"\n"

    rsync -v -a --delete -b --backup-dir=${DIRECTORY_INCREMENTAL_PREFIX}`date +%F` --exclude=${DIRECTORY_INCREMENTAL_PREFIX}* ${directory_source} ${directory_destination}
    if [ $? -eq 0 ]; then
        find ${directory_destination}${DIRECTORY_INCREMENTAL_PREFIX}* -mtime +${EXPIRED_DAYS} -exec rm -rf '{}' ';' 2>/dev/null

        echo -n 'Completed'
    else
        echo -n ${COLOR_RED}'Fail'${FONT_RESET}'ed'
    fi
    time_duration=$(echo "$(date +%s.%N) - $time_start" | bc)
    printf " in %.3f seconds.\n" $time_duration
done
IFS=$IFSOLD
